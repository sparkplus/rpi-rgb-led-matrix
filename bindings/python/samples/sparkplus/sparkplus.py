import os
import datetime
import time
import random
import sys
from PIL import Image
from rgbmatrix import RGBMatrix, RGBMatrixOptions
from multiprocessing import Process, Value
from gpio_reader import read_gpio
import RPi.GPIO as GPIO

IMG_TIME_TABLE = "image-time-table-test.txt"
IMG_UPDATE_TIME = 10000
img_width = 384
img_height = 32
motion = 1


# 시간 구간과 이미지 파일 이름을 파싱하는 함수
# time_slot = [start, end, fileNames,,,]
def parse_time_slots(filename):
    time_slots = []
    with open(filename, "r") as file:
        for line in file:
            # 라인의 앞뒤 공백을 제거
            stripped_line = line.strip()
            
            # 라인이 '#'으로 시작하거나, 공백이나 탭만 포함된 경우 다음 줄로 넘어감
            if stripped_line.startswith('#') or not stripped_line:
                continue
            
            # '#'이 나오기 전까지의 내용만 유효하다고 가정하고 처리
            valid_part = stripped_line.split('#')[0].strip()
            if not valid_part:  # '#' 이전에 내용이 없으면 다음 줄로 넘어감
                continue
            
            parts = valid_part.split()
            # parts = line.strip().split()
            start_time = datetime.datetime.strptime(parts[0], "%H:%M").time()
            end_time = datetime.datetime.strptime(parts[1], "%H:%M").time()
            img_names = parts[2:]  # text of image file name
            time_slots.append((start_time, end_time, img_names))
    return time_slots


# 현재 시간 구간에 해당하는 이미지 목록을 찾는 함수
# time_slot = [start, end, fileNames,,,]
def find_current_time_slot(time_slots, current_time):
    for start_time, end_time, img_names in time_slots:
        if start_time <= end_time:
            # start_time이 end_time보다 작거나 같은 정상적인 경우
            if start_time <= current_time < end_time:
                return img_names
        else:
            # start_time이 end_time보다 큰 경우 (예: 밤에서 새벽으로 넘어가는 경우)
            if current_time >= start_time or current_time < end_time:
                return img_names
    return None


# 메인 GUI 애플리케이션 클래스
class ImageDisplayApp:
    def __init__(self, matrix, time_slots, motion):
        self.matrix = matrix
        self.time_slots = time_slots
        self.motion = motion
        self.curr_img_names = None
        self.past_index = None        
        self.last_index = None
        self.image_index = None
        self.state_display = 0
        # self.update_image()
        
    # 랜덤 번호 구하기, 바로전 번호 제외 (표시할 이미지 번호)
    def select_random(self, start, end, last):
        if 0 == end:
            return 0
        
        if last is None:
            return random.randint(start, end)  # end 포함
            
        now = last
        while now == last:
            now = random.randint(start, end)
        return now
            
    # 랜덤 번호 구하기, 과거 2개 번호 제외 (표시할 이미지 번호)
    def select_random2(self, start, end, past, last):
        if 0 == end:
            return 0
        
        if last is None:
            return random.randint(start, end)  # end 포함
            
        numbers = list(range(end + 1))
        if past is not None:
            numbers.remove(past)
        if last is not None:
            numbers.remove(last)
        return random.choice(numbers)
        
    # 이미지 로드 및 표시
    def display_image(self, image_path):
        img = Image.open(image_path).convert('RGB')
        self.matrix.SetImage(img)

    # 모션 상태 모니터링
    # 시간대에 맞는 이미지 리스트 가져오기
    # 랜덤으로 이미지 표시하기
    def update_image(self):
        while True:
            if 0 == self.motion.value or 0 == self.state_display:
                if 2 != self.state_display:
                    image_path = os.path.join("images", "LED_Default.png")
                    self.display_image(image_path)
                    self.state_display = 2
                    print('Default Image, No person')
                    time.sleep(2.0)
                else:
                    time.sleep(0.1)
                
            elif 1 == self.motion.value:
                current_time = datetime.datetime.now().time()
                # 리스트 반환 (text of image file name) 
                new_img_names = find_current_time_slot(self.time_slots, current_time)  # list or None
                
                if new_img_names != self.curr_img_names:
                    # 다른 시간대 이미지들
                    self.curr_img_names = new_img_names
                    self.image_index = None
                
                if self.curr_img_names:
                    # self.image_index = self.select_random(0, len(self.curr_img_names)-1, self.image_index)
                    self.image_index = self.select_random2(0, len(self.curr_img_names)-1, self.past_index, self.last_index)
                    print(f'index {self.image_index}')
                    image_path = os.path.join("images", self.curr_img_names[self.image_index])
                    self.display_image(image_path)
                    self.state_display = 1
                    self.past_index = self.last_index;
                    self.last_index = self.image_index;
                    time.sleep(10.0)
                else:
                    if 2 != self.state_display:
                        image_path = os.path.join("images", "LED_Default.png")
                        self.display_image(image_path)
                        self.state_display = 2
                        print('Default Image, No time slot')
                        time.sleep(2.0)
                    else:
                        time.sleep(0.1)
            

# MAIN PROGRAM
def main():
    
    # multiprocessing.Value를 사용하여 프로세스 간 공유 변수 생성
    motion = Value('i', 0)  # 'i'는 int 타입을 의미함

    # GPIO 읽기 로직을 실행하는 별도의 프로세스 생성
    p = Process(target=read_gpio, args=(motion,))
    p.start()
    
    # Configuration for the matrix
    options = RGBMatrixOptions()
    options.cols = 64
    options.rows = 32
    options.chain_length = 6
    options.parallel = 1
    options.gpio_slowdown = 4
    options.pwm_lsb_nanoseconds = 130
    options.led_rgb_sequence = 'RBG'
    options.hardware_mapping = 'regular'  # If you have an Adafruit HAT: 'adafruit-hat'
    matrix = RGBMatrix(options=options)
        
    print(datetime.datetime.now())
    time_slots = parse_time_slots(IMG_TIME_TABLE)
    
    try:
        app = ImageDisplayApp(matrix, time_slots, motion)
        app.update_image()
    except :
        print("User stopped")
    finally:
        p.terminate()  # 별도 프로세스 종료
        GPIO.cleanup()

if __name__ == "__main__":
    main()
