from multiprocessing import Value
import time
from datetime import datetime
import RPi.GPIO as GPIO
from threading import Timer

# 상수 정의
GPIO_PIN = 15
READ_INTERVAL = 0.098  # 98ms
TIMER_DURATION = 5     # seconds

# 전역 변수
timer = None

# GPIO 설정
GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_PIN, GPIO.IN)


def print_motion(msg='\n'):
    '''
    # 현재 시간을 초 단위로 가져오기
    now = time.time()
    # 현재 초를 정수로 변환하고 1000으로 나눈 나머지를 계산하여 마지막 3자리만 추출
    seconds = int(now) % 1000
    # 현재 시간(초)의 소수점 첫째 자리(1/100초)와 함께 포맷하여 출력
    formatted_time = f'{seconds:03}.{int((now - int(now)) * 10)}'
    print(formatted_time, end='')  
    '''
    now = datetime.now()
    print(f"{now:%Y-%m-%d %H:%M:%S}.{str(now.microsecond)[0]}", end=msg)  
    
    
def reset_motion(motion):
    with motion.get_lock():  # Value에 대한 lock을 얻어 안전하게 값을 변경
        motion.value = 0
    
    print('Keep Hi 5s  ', end='')
    print_motion()
   
        
def motion_detected(motion):
    global timer
    with motion.get_lock():
        motion.value = 1
        
    print('Detected    ', end='')
    print_motion()     

    # 기존 타이머가 있으면 취소
    if timer is not None:
        timer.cancel()
            
    # 새 타이머 시작
    timer = Timer(TIMER_DURATION, reset_motion, [motion])
    timer.start()


def read_gpio(motion):
    while True:
        if GPIO.input(GPIO_PIN):
            motion_detected(motion)
        time.sleep(READ_INTERVAL)