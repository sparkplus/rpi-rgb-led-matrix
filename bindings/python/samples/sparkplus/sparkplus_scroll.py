import os
import time
from datetime import datetime
import random
import sys
from PIL import Image
from rgbmatrix import RGBMatrix, RGBMatrixOptions
from multiprocessing import Process, Value
from gpio_reader import read_gpio, print_motion
import RPi.GPIO as GPIO


# 전역 변수
IMG_TIME_TABLE = 'images/image-time-table.txt'
IMG_DIRECTORY  = 'images'
IMG_UPDATE_TIME = 10000
NORMAL_IMAGE  = 1
DEFAULT_IMAGE = 2
img_width = 384
img_height = 32


# key가 소문자로 이루어진 index 딕셔너리 만들기
def build_file_index(directory):
    index = {}  # dictionary {key:value}
    for exist_filename in os.listdir(directory):
        index[exist_filename.lower()] = os.path.join(directory, exist_filename)  
        # key는 실제 존재하는 파일이름의 all소문자 : value는 실제 존재하는 파일 이름
        # ex) led.png : LED.png
    return index


# filename이 실제 존재하는지 검색
def find_file(index, filename):
	# index에서 filename을 소문자로 변경한 후 검색, 없으면 None
    return index.get(filename.lower(), None)
    

# 시간 구간과 이미지 파일 이름을 파싱해서 list로 만듬
# time_slot = [start, end, fileNames,,,]
def parse_time_slots(filename):
    time_slots = []
    with open(filename, 'r') as file:
        for line in file:
            # 라인의 앞뒤 공백을 제거
            stripped_line = line.strip()
            
            # 라인이 '#'으로 시작하거나, 공백이나 탭만 포함된 경우 다음 줄로 넘어감
            if stripped_line.startswith('#') or not stripped_line:
                continue
            
            # '#'이 나오기 전까지의 내용만 유효하다고 가정하고 처리
            valid_part = stripped_line.split('#')[0].strip()
            if not valid_part:  # txt 내에서 '#' 이전에 내용이 없으면 다음 줄로 넘어감
                continue
            
            parts = valid_part.split()
            # parts = line.strip().split()
            start_time = datetime.strptime(parts[0], '%H:%M').time()
            end_time = datetime.strptime(parts[1], '%H:%M').time()
            img_names = parts[2:]  # text of image file name
            time_slots.append((start_time, end_time, img_names))
    return time_slots


# 현재 시간 구간에 해당하는 이미지 목록을 찾는 함수
# time_slot = [start, end, fileNames,,,]
def find_current_time_slot(time_slots, current_time):
    for start_time, end_time, img_names in time_slots:
        # 시작시간 종료시간 가져오기
        if start_time <= end_time:
            # start_time이 end_time보다 작거나 같은 정상적인 경우
            if start_time <= current_time < end_time:
                return img_names
        else:
            # start_time이 end_time보다 큰 경우 (예: 밤에서 새벽으로 넘어가는 경우)
            if current_time >= start_time or current_time < end_time:
                return img_names
    return None


# 메인 GUI 애플리케이션 클래스
class ImageDisplayApp:
    # 생성자
    def __init__(self, matrix, time_slots, file_index, motion):
        self.matrix = matrix
        self.time_slots = time_slots
        self.file_index = file_index
        self.motion = motion
        self.curr_img_names = None
        self.past_index = None        
        self.last_index = None
        self.image_index = None
        self.state_display = 0
        
    # 랜덤 번호 구하기, 바로전 번호 제외 (표시할 이미지 번호)
    def select_random(self, start, end, last):
        if 0 > end:
            return 0
        
        if last is None:
            return random.randint(start, end)  # end 포함, 첫 실행
            
        now = last
        while now == last:  # 이전 값과 같은 결과라면 다시 구하라
            now = random.randint(start, end)
        return now
            
    # 랜덤 번호 구하기, 과거 2개 번호 제외 (표시할 이미지 번호)
    def select_random2(self, start, end, past, last):
        if 0 == end:
            return 0
        
        if last is None:
            return random.randint(start, end)  # end 포함, 첫 실행
            
        numbers = list(range(end + 1))  # list 0 ~ end
        if past is not None:
            numbers.remove(past)  # list에서 past 제거
        if last is not None:
            numbers.remove(last)  # list에서 last 제거
        return random.choice(numbers)  # list에서 랜덤 구하기
        
    # 이미지 로드 및 표시
    def display_image(self, path, filename):
        found_path = find_file(self.file_index , filename)
        if found_path:
            image = Image.open(found_path).convert('RGB')
        else:  # None
            image_path = os.path.join(path, 'LED_Default.png')
            image = Image.open(image_path).convert('RGB')  # default image
        self.matrix.SetImage(image)
        
    # 이미지 로드 및 스크롤
    def display_scroll(self, path, filename):
        black = Image.new('RGB', (384, 32), color = 'black')
        found_path = find_file(self.file_index , filename)
        if found_path:
            image = Image.open(found_path).convert('RGB')
        else:  # None
            image_path = os.path.join(path, 'LED_Default.png')
            image = Image.open(image_path).convert('RGB')  # default image
        
        double_buffer = self.matrix.CreateFrameCanvas()
        img_width, img_height = image.size

        # let's scroll
        xpos = 0
        while True:
            xpos += 1
            if (xpos > img_width):
                xpos = 0
                
            double_buffer.SetImage(black, -xpos)
            double_buffer.SetImage(image, -xpos + img_width)

            double_buffer = self.matrix.SwapOnVSync(double_buffer)
            time.sleep(0.004)  # speed (0.003 ~ 0.05)

            if (0 == xpos):
                break

    # 모션 상태 모니터링 motion.value
    # 모션 상태 모니터링
    # 시간대에 맞는 이미지 리스트 가져오기
    # 랜덤으로 이미지 표시하기
    def update_image(self):
        global NORMAL_IMAGE, DEFAULT_IMAGE
        while True:
            if 0 == self.motion.value or 0 == self.state_display:
                if DEFAULT_IMAGE != self.state_display:
                    self.display_image(IMG_DIRECTORY, 'LED_Default.png')
                    self.state_display = DEFAULT_IMAGE
                    print('default 10s ', end='')
                    print_motion('\n\n')
                    time.sleep(2.0)
                else:
                    time.sleep(0.1)
                
            elif 1 == self.motion.value:
                current_time = datetime.now().time()
                # 리스트 반환 (text of image file name) 
                new_img_names = find_current_time_slot(self.time_slots, current_time)  # list or None
                
                if new_img_names != self.curr_img_names:
                    # 다른 시간대 이미지들
                    self.curr_img_names = new_img_names
                    self.image_index = None
                
                if self.curr_img_names:
                    # 과거 2개 메시지번호 제외한 랜덤 번호 구하기
                    self.image_index = self.select_random2(0, len(self.curr_img_names)-1, self.past_index, self.last_index)
                    self.display_image(IMG_DIRECTORY, self.curr_img_names[self.image_index])
                    print(f'\n  image [{self.image_index}] ', end='')
                    print_motion()
                    self.state_display = NORMAL_IMAGE
                    self.past_index = self.last_index;
                    self.last_index = self.image_index;
                    time.sleep(10.0)
                else:
                    if DISPLAY_DEFAULT != self.state_display:
                        self.display_image(IMG_DIRECTORY, 'LED_Default.png')
                        self.state_display = DEFAULT_IMAGE
                        print('No timeslot ', end='')
                        print_motion()
                        time.sleep(2.0)
                    else:
                        time.sleep(0.1)
            

# MAIN PROGRAM
def main():
    global motion
    
    print(datetime.now())
        
    # multiprocessing.Value를 사용하여 프로세스 간 공유 변수 생성
    motion = Value('i', 0)  # 'i'는 int 타입을 의미함

    # GPIO 읽기 로직을 실행하는 별도의 프로세스 생성
    p = Process(target=read_gpio, args=(motion,))
    p.start()
    
    # Configuration for the matrix
    options = RGBMatrixOptions()
    options.cols = 64
    options.rows = 32
    options.chain_length = 6
    options.parallel = 1
    options.gpio_slowdown = 4
    options.pwm_lsb_nanoseconds = 130
    options.led_rgb_sequence = 'RBG'
    options.hardware_mapping = 'regular'  # If you have an Adafruit HAT: 'adafruit-hat'
    matrix = RGBMatrix(options=options)
        
    # images 폴더 파일들을 dirctionary 로 반환
    file_index = build_file_index('images')
    
    # 시작, 종료, 이미지파일 이름들을 parsing해서 list로 반환
    time_slots = parse_time_slots(IMG_TIME_TABLE)
    
    try:
        app = ImageDisplayApp(matrix, time_slots, file_index, motion)
        app.update_image()
    except :
        print('User stopped')
    finally:
        p.terminate()  # 별도 프로세스 종료
        GPIO.cleanup()

if __name__ == '__main__':
    main()
