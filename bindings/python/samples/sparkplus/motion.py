import time
from multiprocessing import Process, Value
from gpio_reader import read_gpio
import RPi.GPIO as GPIO


def main():
    # multiprocessing.Value를 사용하여 프로세스 간 공유 변수 생성
    motion = Value('i', 0)  # 'i'는 int 타입을 의미함

    # GPIO 읽기 로직을 실행하는 별도의 프로세스 생성
    p = Process(target=read_gpio, args=(motion,))
    p.start()
    
    try:
        # 메인 프로세스는 다른 작업을 계속할 수 있음
        # 예시: 공유 변수 `motion`의 값을 모니터링
        
        # 부모 프로세스 loop
        while True:
            time.sleep(0.1)  # 실제 애플리케이션에 맞게 조정
            # print(f"Motion value in main process: {motion.value}")
            
    except KeyboardInterrupt:
        print("Program stopped")
    finally:
        p.terminate()  # 별도 프로세스 종료
        GPIO.cleanup()


if __name__ == "__main__":
    main()
