#!/usr/bin/env python
import time
import sys
from rgbmatrix import RGBMatrix, RGBMatrixOptions
from PIL import Image

def main():
    if len(sys.argv) < 2:
        sys.exit("Require an image argument")
    else:
        image_file = sys.argv[1]

    image = Image.open(image_file)

    # Configuration for the matrix
    options = RGBMatrixOptions()
    options.cols = 64
    options.rows = 32
    options.chain_length = 6
    options.parallel = 1
    options.gpio_slowdown = 4
    options.pwm_lsb_nanoseconds = 130
    options.led_rgb_sequence = 'RBG'
    options.hardware_mapping = 'regular'  # If you have an Adafruit HAT: 'adafruit-hat'

    matrix = RGBMatrix(options=options)

    # Make image fit our screen.
    image.thumbnail((matrix.width, matrix.height), Image.LANCZOS)

    matrix.SetImage(image.convert('RGB'))  # RGB

    try:
        print("Press CTRL-C to stop.")
        while True:
            time.sleep(100)
    except KeyboardInterrupt:
        sys.exit(0)

if __name__ == "__main__":
    main()
